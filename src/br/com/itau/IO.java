package br.com.itau;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class IO{

    public static Map<String,String> solicitarDados(){
        Scanner scanner = new Scanner(System.in);

        System.out.println("Digite a quantidade de Números: ");
        String qtdnumeros = scanner.nextLine();

        System.out.println("Digite a quantidade de vezes: ");
        String qtdvezes = scanner.nextLine();

        Map<String, String> dados = new HashMap<>();
        dados.put("qtdnumeros", qtdnumeros);
        dados.put("qtdvezes", qtdvezes);

        return dados;
    }

    public static void imprimir(Resultado resultado){
        for (int numero : resultado.getNumeros()) {
            System.out.print(numero);
            System.out.print(", ");
        }

        System.out.println(resultado.getSoma());
    }

    public static void imprimir(Resultado[] resultados){
        for (Resultado resultado : resultados) {
            imprimir(resultado);
        }
    }
}
