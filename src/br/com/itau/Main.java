package br.com.itau;

import br.com.itau.Aleatorio;
import br.com.itau.Dado;
import br.com.itau.IO;
import br.com.itau.Resultado;

import java.util.Map;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Map<String, String> dados = IO.solicitarDados();
        Scanner in = new Scanner(System.in);

        Dado dado = new Dado(6);

        Resultado[] resultados = Aleatorio.sortearGrupos(
                dado,
                Integer.parseInt(dados.get("qtdnumeros")),
                Integer.parseInt(dados.get("qtdvezes"))

        );

                IO.imprimir(resultados);
    }
}


